const AWS = require("ibm-cos-sdk");
const config = require("../config");

var cos = new AWS.S3(config);

module.exports = cos;
