var cos = require("./index");
/* 
NOTE: Doesn't have all functions available by IBM COS SDK, because this was just a test
*/
exports.createBucket = bucketName => {
  console.log(`Creating new bucket: ${bucketName}`);
  return cos
    .createBucket({
      Bucket: bucketName,
      CreateBucketConfiguration: {
        LocationConstraint: "au-syd-standard"
      }
    })
    .promise()
    .then(() => {
      console.log(`Bucket: ${bucketName} created!`);
    })
    .catch(e => {
      console.error(`ERROR: ${e.code} - ${e.message}\n`);
    });
};

exports.deleteBucket = bucketName => {
  console.log(`Deleting bucket: ${bucketName}`);
  return cos
    .deleteBucket({
      Bucket: bucketName
    })
    .promise()
    .then(() => {
      console.log(`Bucket: ${bucketName} deleted!`);
    })
    .catch(e => {
      console.error(`ERROR: ${e.code} - ${e.message}\n`);
    });
};

exports.getBuckets = () => {
  console.log("Retrieving list of buckets");
  return cos
    .listBuckets()
    .promise()
    .then(data => {
      if (data.Buckets != null) {
        for (var i = 0; i < data.Buckets.length; i++) {
          console.log(`Bucket Name: ${data.Buckets[i].Name}`);
        }
      }
    })
    .catch(e => {
      console.error(`ERROR: ${e.code} - ${e.message}\n`);
    });
};
